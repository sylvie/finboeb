require('dotenv').config()

const express = require("express");
const fetch = require("node-fetch");
const calc = require("./calcValue.js");
const cookieParse = require("set-cookie-parser");

const app = express();

const sheetsApiLink =
  "https://gsx2json.herokuapp.com/api?id=1pZzhw2BacHz2P6mQtfJDHgPqGnHZ_H2uFwRDrh1Ornw&columns=false";

let finobeSession = "lol";

const handleFirst = async function(request, response, next) {
  if (finobeSession === "lol") await refreshCookie(response);

  console.log("Getting " + request.params.username);

  let userId = await fetch(
    `https://finobe.com/api/user/get/idfromname?name=${request.params.username}`
  ).then(res => res.json());

  if (userId.status == "notfound") {
    response.send("User not found.");
    response.end();
    return;
  }

  let itemsResp;

  try {
    itemsResp = await fetch(`https://finobe.com/api/trade/${userId.id}`, {
      headers: {
        cookie: `finobe_session=${finobeSession}`
      }
    }).then(res => res.json());
  } catch (err) {
    console.log(err);
    refreshCookie(response);
    return;
  }

  let valueListResp = await fetch(sheetsApiLink)
    .then(res => res.json())
    .then(json => json.rows);

  let items = itemsResp.to.items;

  response.locals.invData = { items, valueListResp };
  next();
};

const refreshCookie = async response => {
  let itemsResp = await fetch(`https://finobe.com/api/trade/1200`, {
    headers: {
      cookie: process.env.FREMEMBER
    }
  }).catch(() => {
    response.status(400);
    response.send("Finob Down");
    response.end();
    return;
  });

  if (itemsResp.url.includes("/login")) {
    response.status(400);
    response.send(
      "Error: You really shouldn't be seeing this. Refresh, and then if you see this spam ping sylvie :)."
    );
    response.end();
    return;
  }

  let finobeCookie = cookieParse(itemsResp.headers.raw()["set-cookie"]).filter(
    x => x.name == "finobe_session"
  )[0].value;

  console.log("Cookie set");

  finobeSession = finobeCookie;
  return;
};

// Get Trolled MotherFucker
app.get("/", (request, response) => {
  response.redirect("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
});

app.get("/getValue/:username", handleFirst, async (request, response) => {
  let { items, valueListResp } = response.locals.invData;

  let results = calc(items, valueListResp);

  if (request.query.json == 1) response.send(results);

  response.send(`**${request.params.username}**<br/>
    Total Items: ${items.length}<br/>
    Value: ${results.value}<br/>
    RAP: ${results.rap}<br/>
    Total: ${results.total}`);
});

app.get("/getItems/:username", handleFirst, async (request, response) => {
  let { items, valueListResp } = response.locals.invData;

  let results = calc(items, valueListResp, true);
  
  if (request.query.json == 1) response.send(results);

  let itemStr = `Username: ${request.params.username}<br/><br/>`;

  results.forEach(item => {
    itemStr += `${item.name}: ${item.value}<br/>`;
  });

  response.send(itemStr);
});

app.get("/refreshCookie", async (request, response) => {
  await refreshCookie(response);
  response.send("Cookie Refreshed :D");
});

// listen for requests :)
const listener = app.listen(process.env.PORT, "localhost", () => {
  console.log("Your app is listening on port " + listener.address().port);
});
