let valuecalc = function(rawItems, valueList, returnItems = false) {
  let items = rawItems.map(userItem => {
    let id = userItem.asset_id;
    let valueFiltered = valueList.filter(value => {
      return value.itemid === id;
    });

    let rap;
    let isValued;

    if (valueFiltered.length > 0) {
      rap = valueFiltered[0].value;
      isValued = true;
    } else {
      rap = userItem.average_price;
      isValued = false;
    }

    return { id, rap, isValued, name: userItem.asset_name };
  });

  let ownedValue = Object.values(items)
    .filter(x => x.isValued)
    .map(n => n.rap)
    .reduce((acc, cur) => acc + cur, 0);

  let ownedRap = Object.values(items)
    .filter(x => !x.isValued)
    .map(n => n.rap)
    .reduce((acc, cur) => acc + cur, 0);

  if (returnItems === true)
    return Object.values(items)
      .map(x => ({
        name: x.name,
        value: x.rap,
        id: x.id
      }))
      .sort((a, b) => {
        return b.value - a.value;
      });

  return {
    totalItems: items.length,
    value: ownedValue,
    rap: ownedRap,
    total: ownedRap + ownedValue
  };
};

module.exports = valuecalc;
